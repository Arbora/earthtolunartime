<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    public function boot()
    {
        app('validator')->extend('dateTimeUTC', function ($attribute, $value) {
            try {
                $dt = new \DateTime($value);
                $dt->format(\DateTime::ATOM); // contains valid date
                return true;
            }catch(\Exception $exception) {
                return false;
            }
        });
    }
}
