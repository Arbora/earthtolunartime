<?php

namespace App\Http\Controllers;

use App\Libraries\ArrivalTimeCalculator;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;

class TimeController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/utc2lst/{time}",
     *     operationId="/api/utc2lst/time",
     *     @OA\Parameter(
     *         name="time",
     *         in="path",
     *         description="The time parameter in UTC format",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Returns the time in LST format",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Error: Bad request. When required parameters were not supplied or incorrect",
     *     ),
     * )
     * @param $datetime
     * @return JsonResponse
     */

    public function utc2lst($datetime): JsonResponse
    {
        $this->validateDatetime($datetime);
        return response()->json([
            'LST' => (new ArrivalTimeCalculator())
                ->setTimeFromWarehouseToTheSpaceStation('+3 days')
                ->setTimeFromEarthToTheMoon('+1 day')
                ->toLST($datetime)
        ]);
    }

    private function validateDatetime($datetime)
    {
        $validator = Validator::make(
            ['datetime' => $datetime],
            [
                'datetime' => 'required|dateTimeUTC'
            ]
        );
        $validator->validate();
    }

}
