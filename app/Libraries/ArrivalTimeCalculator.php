<?php

namespace App\Libraries;

use Carbon\Carbon;

class ArrivalTimeCalculator
{
    private const MOON_START_TIME_UTC = '1969-07-21T02:56:15Z';
    private const MOON_SECOND = 29.530589/30;

    private string $timeFromWarehouseToTheSpaceStation;
    private string $timeFromEarthToTheMoon;

    public function setTimeFromWarehouseToTheSpaceStation(string $value): self
    {
        $this->timeFromWarehouseToTheSpaceStation = $value;
        return $this;
    }

    public function setTimeFromEarthToTheMoon(string $timeFromEarthToTheMoon): self
    {
        $this->timeFromEarthToTheMoon = $timeFromEarthToTheMoon;
        return $this;
    }

    public function toLST(string $departureTimeUTC): string
    {
        // Get arrival time in UTC
        $arrivalTS = (new Carbon($departureTimeUTC))
            // adding time
            ->add($this->timeFromWarehouseToTheSpaceStation)
            ->add($this->timeFromEarthToTheMoon);

        // Calc the time between departure and a date the moon started
        $totalSeconds = $arrivalTS->diffInSeconds(self::MOON_START_TIME_UTC);

        // How many MOON seconds passed?
        $moonSeconds = $totalSeconds / self::MOON_SECOND;

        return $this->moonSecondsToDateTime($moonSeconds)
            ->format('y-m-d ∇ H:i:s');
    }

    private function moonSecondsToDateTime($moonSeconds): Carbon
    {
        $years = (int) ($temp = $moonSeconds / (60*60*24*30*12));
        $months = (int) ($temp = ($temp - $years) * 12);
        $days = (int) ($temp = ($temp - $months) * 30);
        $hours = (int) ($temp = ($temp - $days) * 24);
        $minutes = (int) ($temp = ($temp - $hours) * 60);
        $seconds = (int) ($temp = ($temp - $minutes) * 60);
        return Carbon::create($years+1, $months+1, $days+1, $hours, $minutes, $seconds);
    }

}
