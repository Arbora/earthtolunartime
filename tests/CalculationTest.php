<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class CalculationTest extends TestCase
{
    /**
     * /api/UTC2LTS/UTC [GET]
     */
    public function testShouldReturnTimeInLTS()
    {
        $this->get("api/utc2lst/2021-09-13T14:15:16Z", []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure(
            ['LST']
        );
        // '{"LST":"54-10-05 ∇ 07:25:02"}'
        $this->seeJsonEquals([
            'LST' => "54-10-05 ∇ 07:25:02"
        ]);
    }
}
